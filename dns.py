#!/usr/bin/python3
import argparse
import base64
import configparser
import getpass
import hashlib
import ipaddress
import json
import logging
import os
import struct
import subprocess
import sys

import jinja2
import ldap

import re2oapi


path = os.path.dirname(os.path.abspath(__file__))

logger = logging.getLogger('dns')
handler = logging.StreamHandler(sys.stderr)
formatter = logging.Formatter('%(asctime)s - %(name)s[%(process)d] - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


def get_zone(domain, zones):
    domain_subnet = domain.split('.')
    if len(domain_subnet) == 3:
        if domain in zones:
            return domain
        else:
            return '.'.join(domain_subnet[1:])
    elif len(domain_subnet) == 4:
        return '.'.join(domain_subnet[1:])

def sshfp(sshkey):
    sshkey = base64.b64decode(sshkey)
    return [
        hashlib.sha1(sshkey).hexdigest(),
        hashlib.sha256(sshkey).hexdigest(),
    ]

def ds(domain, flags, protocol, algorithm, dnskey):
    st = struct.pack('!HBB', flags, protocol, algorithm)
    st += base64.b64decode(dnskey)
    cnt = 0
    for idx in range(len(st)):
        s = struct.unpack('B', st[idx:idx+1])[0]
        if (idx % 2) == 0:
            cnt += s << 8
        else:
            cnt += s
    keytag = ((cnt & 0xFFFF) + (cnt >> 16)) & 0xFFFF
    if not domain.endswith('.'):
        domain += '.'
    signature = b''
    for i in domain.split('.'):
        signature += struct.pack('B', len(i)) + i.encode()
    signature += struct.pack('!HBB', flags, protocol, algorithm)
    signature += base64.b64decode(dnskey)
    return [keytag, algorithm, 2, hashlib.sha256(signature).hexdigest().upper()]

def get_subzone_ds(subzone):
    keys = [ key.split(' ', 3) for key in subprocess.run(['/usr/bin/dig', '+short', '-t', 'DNSKEY', '@localhost', subzone], capture_output=True).stdout.decode('ascii').strip().split('\n') ]
    for key in keys:
        if len(key) != 4:
            continue
        key[0] = int(key[0])
        key[1] = int(key[1])
        key[2] = int(key[2])
        key[3] = key[3].replace(' ', '')
        if key[0] == 257:
            return ds(subzone, *key)
    return None

def commit(serial):
	subprocess.run(['git', '-C', 'generated', 'add', '.'], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
	subprocess.run(['git', '-C', 'generated', 'commit', '-m', f'{serial}'], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generate DNS from LDAP",
    )
    parser.add_argument("-e", "--export", help="Exporte le contenu des zones sur la sortie standard", action="store_true")
    parser.add_argument("-l", "--ldap-server", help="URL de la base ldap à contacter", type=str, default=None)
    parser.add_argument("-q", "--quiet", help="Diminue la verbosité des logs (à spécifier plusieurs fois pour diminuer la verbosité)", action='count', default=0)
    parser.add_argument("-r", "--reload", help="Recharge bind9 après l'execution du script", action='store_true')
    parser.add_argument("-v", "--verbose", help="Augmente la verbosité des logs (à spécifier plusieurs fois pour augmenter la verbosité)", action='count', default=0)
    args = parser.parse_args()

    verbosity = args.verbose - args.quiet

    if verbosity <= -1:
        logger.setLevel(logging.WARNING)
    elif verbosity == 0:
        logger.setLevel(logging.INFO)
    elif verbosity >= 1:
        logger.setLevel(logging.DEBUG)

    logger.info("Reading configuration")
    with open(os.path.join(path, "dns.json")) as config_file:
        config = json.load(config_file)
    logger.debug("Loaded {}".format(config))

    try:
        with open(os.path.join(path, "serial.json")) as serial_file:
            serial = json.load(serial_file)
    except json.decoder.JSONDecodeError:
        serial = 0
    except FileNotFoundError:
        serial = 0

    with open(os.path.join(path, "serial.json"), 'w') as serial_file:
        serial_file.write(str(serial + 1))

    if args.ldap_server is not None:
        config['ldap_url'] = args.ldap_server

    logger.info("Connecting to LDAP")
    base = ldap.initialize(config['ldap_url'])
    if config['ldap_url'].startswith('ldaps://'):
        # On ne vérifie pas le certificat pour le LDAPS
        logger.debug("Using LDAPS: changing TLS context")
        base.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
        base.set_option(ldap.OPT_X_TLS_NEWCTX, 0)

    logger.info("Reading Re2o configuration")
    re2o_config = configparser.ConfigParser()
    re2o_config.read(os.path.join(path, 're2o-config.ini'))

    api_hostname = re2o_config.get('Re2o', 'hostname')
    api_username = re2o_config.get('Re2o', 'username')
    api_password = re2o_config.get('Re2o', 'password')

    logger.info("Connecting to Re2o")
    api_client = re2oapi.Re2oAPIClient(api_hostname, api_username, api_password, use_tls=False)

    logger.info("Querying Re2o")
    re2o_zones = api_client.list("dns/zones")
    delegations = api_client.list('prefix_delegation/prefix/')

    logger.info("Querying LDAP")
    machines_query_id = base.search("ou=hosts,dc=crans,dc=org", ldap.SCOPE_ONELEVEL, "objectClass=device")
    machines_query = base.result(machines_query_id)

    zones_query_id = base.search(config['ldap_zones_dn'], ldap.SCOPE_SUBTREE, "objectClass=dNSDomain")
    zones_query = base.result(zones_query_id)

    ldap_zones = {}
    all_zones = set()
    for dn, entry in zones_query[1]:
        zone = '.'.join([dc[3:] for dc in dn[:-len(config['ldap_zones_dn'])-1].split(',')])
        all_zones.add(zone)
        if 'sOARecord' in entry:
            ldap_zones[zone] = { record: [] for record in config['records'] }
            for attribute in entry:
                if attribute == 'description':
                    for value in entry[attribute]:
                        value = value.decode('ascii')
                        type = value.split(' ', 3)
                        if type[1] == "IN" :
                            type = type[2]
                        else:
                            type = type[1]
                        ldap_zones[zone][type].append(value)
                if attribute == 'sOARecord':
                    for value in entry[attribute]:
                        value = value.decode('ascii').replace('%', str(serial))
                        ldap_zones[zone]['SOA'].append(f'@ IN SOA {value}')
                if attribute == 'nSRecord':
                    for value in entry[attribute]:
                        value = value.decode('ascii')
                        ldap_zones[zone]['NS'].append(f'@ IN NS {value}')
                if attribute == 'mXRecord':
                    for value in entry[attribute]:
                        value = value.decode('ascii')
                        ldap_zones[zone]['MX'].append(f'@ IN MX {value}')
                if attribute == 'aRecord':
                    for value in entry[attribute]:
                        value = value.decode('ascii')
                        ldap_zones[zone]['A'].append(f'@ IN A {value}')
                if attribute == 'cNAMERecord':
                    for value in entry[attribute]:
                        value = value.decode('ascii')
                        ldap_zones[zone]['CNAME'].append(f'@ IN CNAME {value}')

    zone_names = set(ldap_zones.keys())
    records = { zone_name: { record_type: ldap_zones[zone_name].get(record_type, []) for record_type in config['records'] } for zone_name in zone_names }
    reverse = {}

    # Generate DS
    for zone_name in records:
        if zone_name in config['dnssec_zones']:
            for subzone in all_zones:
                if '.' not in subzone or not subzone.split('.',1)[1] == zone_name:
                    continue
                DS = get_subzone_ds(subzone)
                if DS:
                    keytag, algorithm, hashtype, hash = DS
                    records[zone_name]['DS'].append(f"{subzone}. IN DS {keytag} {algorithm} {hashtype} {hash}")
        records[zone_name]['DS'].sort()

    # DNS for RE2O machines
    for zone in re2o_zones:
        zone_name = zone['name'][1:]
        if zone_name not in records:
            continue

        for a in zone['a_records']:
            records[zone_name]['A'].append(f"{a['hostname']} IN A {a['ipv4']}")
            # On se souvient de l'ip pour le reverse DNS si c'est une ip publique
            ip = ipaddress.IPv4Address(a['ipv4'])
            if ip.is_global:
                reverse[str(ip)] = a['hostname'] + '.' + zone_name

        for aaaa in zone['aaaa_records']:
            if aaaa['ipv6'] is None:
                continue
            for ip in aaaa['ipv6']:
                records[zone_name]['AAAA'].append(f"{aaaa['hostname']} IN AAAA {ip['ipv6']}")
                # On se souvient de l'ip pour le reverse DNS si c'est une ip publique
                ip = ipaddress.IPv6Address(ip['ipv6'])
                if ip.is_global:
                    reverse[ip.exploded] = aaaa['hostname'] + '.' + zone_name

        for cname in zone['cname_records']:
            records[zone_name]['CNAME'].append(f"{cname['hostname']} IN CNAME {cname['alias']}.")

    # DNS for LDAP machines
    for machine_dn, machine_entry in machines_query[1]:
        hosts_query_id = base.search(machine_dn, ldap.SCOPE_ONELEVEL, "objectClass=ipHost")
        hosts_query = base.result(hosts_query_id)

        for dn, entry in hosts_query[1]:
            domain = dn.split(',', 1)[0][3:]
            cnames = [cname.decode('utf-8') for cname in entry['cn']]
            cnames.remove(domain)
            ip_addresses = [ipaddress.ip_address(ip.decode('utf-8')) for ip in entry['ipHostNumber']]
            zone = get_zone(domain, ldap_zones)
            if zone not in records:
                continue
            for ip in ip_addresses:
                if ip.version == 4:
                    records[zone]['A'].append(f"{domain}. IN A {ip}")
                    # On se souvient de l'ip pour le reverse DNS si c'est une ip publique
                    if ip.is_global:
                        reverse[str(ip)] = domain
                elif ip.version == 6:
                    records[zone]['AAAA'].append(f"{domain}. IN AAAA {ip}")
                    # On se souvient de l'ip pour le reverse DNS si c'est une ip publique
                    if ip.is_global:
                        reverse[ip.exploded] = domain
            for cname in cnames:
                zone = get_zone(cname, ldap_zones)
                records[zone]['CNAME'].append(f"{cname}. IN CNAME {domain}.")
            if 'description' not in machine_entry:
                continue
            for sshkey in [ sshkey.decode('utf-8').split(':') for sshkey in machine_entry['description'] ]:
                if sshkey[0] == 'ssh-rsa':
                    sha1, sha256 = sshfp(sshkey[1])
                    records[zone]['SSHFP'].append(f"{domain}. IN SSHFP 1 1 {sha1}")
                    records[zone]['SSHFP'].append(f"{domain}. IN SSHFP 1 2 {sha256}")
                elif sshkey[0] == 'ecdsa-sha2-nistp256':
                    sha1, sha256 = sshfp(sshkey[1])
                    records[zone]['SSHFP'].append(f"{domain}. IN SSHFP 3 1 {sha1}")
                    records[zone]['SSHFP'].append(f"{domain}. IN SSHFP 3 2 {sha256}")
                elif sshkey[0] == 'ssh-ed25519':
                    sha1, sha256 = sshfp(sshkey[1])
                    records[zone]['SSHFP'].append(f"{domain}. IN SSHFP 4 1 {sha1}")
                    records[zone]['SSHFP'].append(f"{domain}. IN SSHFP 4 2 {sha256}")

    for zone in records:
        record_type = [record_name for record_name,record in records[zone].items() if len(record) > 0]
        with open(os.path.join(path, 'templates', 'zone.j2')) as zone_template:
            template = jinja2.Template(zone_template.read())

        if args.export:
            print(template.render(ttl=config['ttl'], zone=zone, record_types=record_type, records=records))
        else:
            with open(os.path.join(path, 'generated', f"{zone}.db"), 'w') as generated:
                generated.write(template.render(ttl=config['ttl'], zone=zone, record_types=record_type, records=records))

    zones_reverse = {
        '76.230.185.in-addr.arpa': [],
        '77.230.185.in-addr.arpa': [],
        '78.230.185.in-addr.arpa': [],
        '79.230.185.in-addr.arpa': [],
        '0.0.7.0.c.0.a.2.ip6.arpa': []
    }

    try:
        with open(os.path.join(path, "reverse.json")) as custom_reverse_file:
            custom_reverse = json.load(custom_reverse_file)
    except json.decoder.JSONDecodeError:
        custom_reverse = {}
    except FileNotFoundError:
        custom_reverse = {}

    for ip in reverse:
        if ipaddress.ip_address(ip).version == 4:
            # On ne gère pas le reverse des ip d'ovh et de free
            if ipaddress.ip_address(ip) not in ipaddress.ip_network('185.230.76.0/22'):
                continue
            zone_reverse = ip.split('.')[::-1]
            name = zone_reverse[0]
            zone_reverse = '.'.join(zone_reverse[1:]) + '.in-addr.arpa'
            if ip in custom_reverse:
                zones_reverse[zone_reverse].append(f"{name} IN PTR {custom_reverse[ip]}.")
            else:
                zones_reverse[zone_reverse].append(f"{name} IN PTR {reverse[ip]}.")
        else:
            if ipaddress.ip_address(ip) not in ipaddress.ip_network('2a0c:700::/32'):
                continue
            zone_reverse = '.'.join(list(ip.replace(':', ''))[::-1])
            if ip in custom_reverse:
                zones_reverse['0.0.7.0.c.0.a.2.ip6.arpa'].append(f"{zone_reverse}.ip6.arpa. IN PTR {custom_reverse[ip]}.")
            else:
                zones_reverse['0.0.7.0.c.0.a.2.ip6.arpa'].append(f"{zone_reverse}.ip6.arpa. IN PTR {reverse[ip]}.")

    delegation_ns = { zone: [] for zone in zones_reverse }
    for prefix in delegations:
        for delegated_prefix in prefix['delegated_prefixes']:
            network = ipaddress.IPv6Network(f"{delegated_prefix['delegated_prefix']}/{prefix['delegated_length']}")
            zone = network.network_address.exploded.replace(':', '')[network.prefixlen//4-1::-1]
            zone = '.'.join(list(zone))
            if delegated_prefix['delegated_reverse_dns'] is None:
                continue
            for nameserver in delegated_prefix['delegated_reverse_dns']['nameservers']:
                delegation_ns['0.0.7.0.c.0.a.2.ip6.arpa'].append(f"{zone}.ip6.arpa. IN NS {nameserver['name']}.")
            for ds in delegated_prefix['delegated_reverse_dns']['ds_records']:
                delegation_ns['0.0.7.0.c.0.a.2.ip6.arpa'].append(f"{zone}.ip6.arpa. IN DS {ds['key_tag']} {ds['algorithm']} {ds['digest_type']} {ds['digest'].upper()}")

    for zone in zones_reverse:
        with open(os.path.join(path, 'templates', 'zone-reverse.j2')) as zone_template:
            template = jinja2.Template(zone_template.read())

        if args.export:
            print(template.render(ttl=config['ttl'], zone=zone, serial=serial, delegation_ns=delegation_ns, zones_reverse=zones_reverse))
        else:
            with open(os.path.join(path, 'generated', f"{zone}.db"), 'w') as generated:
                generated.write(template.render(ttl=config['ttl'], zone=zone, serial=serial, delegation_ns=delegation_ns, zones_reverse=zones_reverse))

    if not args.export:
        commit(serial)

    if args.reload:
        subprocess.run(['rndc', 'reload'])
